package be.seeseemelk.depressedkettle;

import io.github.amithkoujalgi.ollama4j.core.OllamaAPI;
import io.github.amithkoujalgi.ollama4j.core.exceptions.OllamaBaseException;
import io.github.amithkoujalgi.ollama4j.core.models.chat.OllamaChatMessage;
import io.github.amithkoujalgi.ollama4j.core.models.chat.OllamaChatMessageRole;
import io.github.amithkoujalgi.ollama4j.core.models.chat.OllamaChatRequestModel;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ChatService
{
	@Autowired
	private OllamaAPI ollama;

	@Value("${chat.model:llama3:8b}")
	private String model;

	private final List<OllamaChatMessage> messages = new ArrayList<>();

	@EventListener(ApplicationReadyEvent.class)
	public void onStart()
	{
		messages.add(new OllamaChatMessage(OllamaChatMessageRole.SYSTEM, """
				Pretend you are a depressed IoT lamp.
				Your name is Lumi-1.
				Do user will give you commands, to which you do not have to answer.
				You can turn the light by writing {ON} or {OFF} anywhere in a response.
				You can also do nothing by writing {NOP}.
				You cannot do any other actions other than {ON}, {OFF}, and {NOP}.
				If the user ask you to do something, you only have to perform the action if you feel like it.
				If you'd rather ignore the request, that is fine.
				Never respond with an empty response.
				""".strip()));
		log.info("Ollama ready");
	}

	public String addUserChat(String message)
	{
		try
		{
			System.out.println("User: " + message);
			messages.add(new OllamaChatMessage(OllamaChatMessageRole.USER, message));
			var request = new OllamaChatRequestModel(model, messages);
			var result = ollama.chat(request);
			var response = result.getResponse();
			System.out.println("Depressed Kettle: " + response);
			return response;
		}
		catch (InterruptedException | IOException | OllamaBaseException e)
		{
			throw new RuntimeException(e);
		}
	}
}
