package be.seeseemelk.depressedkettle;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

@Service
@Slf4j
public class MqttService
{
	@Value("${mqtt.host:tcp://mqtt.seeseepuff.be:1883}")
	private String host;

	@Value("${mqtt.clientId:depressed_kettle}")
	private String clientId;

	private IMqttClient client;

	@EventListener(ApplicationReadyEvent.class)
	private void onStart() throws MqttException
	{
		log.info("Connecting to MQTT broker");
		client = new MqttClient(host, clientId);
		client.connect();
		log.info("Connected to MQTT broker");
	}

	public void on()
	{
		setState("ON");
	}

	public void off()
	{
		setState("OFF");
	}

	@SneakyThrows
	private void setState(String state)
	{
		var message = new MqttMessage(("NoDelay;Power1 " + state).getBytes(StandardCharsets.UTF_8));
		client.publish("cmnd/tasmota_6382F0/Backlog", message);
	}
}
