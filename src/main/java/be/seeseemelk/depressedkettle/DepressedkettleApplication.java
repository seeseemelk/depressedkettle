package be.seeseemelk.depressedkettle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DepressedkettleApplication
{

	public static void main(String[] args)
	{
		SpringApplication.run(DepressedkettleApplication.class, args);
	}

}
