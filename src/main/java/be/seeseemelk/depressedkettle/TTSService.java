package be.seeseemelk.depressedkettle;

import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.sound.sampled.AudioSystem;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class TTSService
{
	@Value("${tts.host:http://localhost:5002}")
	private String host;

	public void play(String message)
	{
		playClip(message);
	}

	@SneakyThrows
	private void playClip(String message)
	{
		var clip = AudioSystem.getClip();
		var parameter = URLEncoder.encode(message, StandardCharsets.UTF_8);
		var url = "http://localhost:5002/api/tts?text=" + parameter + "&speaker_id=&style_wav=&language_id=";
		var mpv = Runtime.getRuntime().exec(new String[]{"mpv", url});
		mpv.waitFor();
		System.out.println("Finished playing audio");
	}
}
