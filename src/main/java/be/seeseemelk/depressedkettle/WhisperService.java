package be.seeseemelk.depressedkettle;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
@Slf4j
public class WhisperService
{
	private static final int ACTIVATION_COUNT_THRESHOLD = 20;
	private static final int DEACTIVATION_COUNT_THRESHOLD = -100;
	private static final double ACTIVATION_ENERGY_THRESHOLD = 1.0;

	@Value("${whisper.host:http://localhost:9000}")
	private String whisperUrl;
	@Autowired
	private AudioFormat format;
	@Autowired
	private TargetDataLine line;

	private final ExecutorService executor = Executors.newSingleThreadExecutor();
	private final AtomicBoolean running = new AtomicBoolean(false);
	private final AtomicBoolean autoEnd = new AtomicBoolean(false);
	private final ByteArrayOutputStream rawAudioData = new ByteArrayOutputStream();
	private final ByteArrayOutputStream formattedAudioData = new ByteArrayOutputStream();

	private WebClient client;
	private CountDownLatch latch;

	@SneakyThrows
	@EventListener(ApplicationReadyEvent.class)
	private void onStart()
	{
		client = WebClient.create(whisperUrl);
	}

	@SneakyThrows
	public void startRecording()
	{
		latch = new CountDownLatch(1);
		rawAudioData.reset();
		line.flush();
		line.start();
		running.set(true);
		autoEnd.set(false);
		executor.submit(this::copyAudioData);
	}

	@SneakyThrows
	public String stopRecording()
	{
		line.stop();
		running.set(false);
		latch.await();
		return submitSound();
	}

	@SneakyThrows
	public String autoStop()
	{
		autoEnd.set(true);
		latch.await();
		line.stop();
		return submitSound();
	}

	@SneakyThrows
	private void copyAudioData()
	{
		boolean reachedStart = false;
		boolean reachedEnd = false;

		int count = 0;

		byte[] buf = new byte[1024];
		while (running.get() && !(autoEnd.get() && reachedEnd))
		{
			var read = line.read(buf, 0, buf.length);
			rawAudioData.write(buf, 0, read);

			double energy = 0;
			for (int i = 0; i < read; i += 4)
			{
				int low = Byte.toUnsignedInt(buf[i]);
				int high = Byte.toUnsignedInt(buf[i+1]);
				energy += (short) (low | (high << 8)) / 32768.0;
			}
			boolean active = Math.abs(energy) > ACTIVATION_ENERGY_THRESHOLD;
			if (!active)
				count = Math.max(DEACTIVATION_COUNT_THRESHOLD, count - 1);
			else
				count = Math.min(ACTIVATION_COUNT_THRESHOLD * 3, count + 3);

			if (count >= ACTIVATION_COUNT_THRESHOLD)
			{
				if (!reachedStart) log.info("Reached start of voice stream");
				reachedStart = true;
			}
			else if (reachedStart && count <= DEACTIVATION_COUNT_THRESHOLD)
			{
				if (!reachedEnd) log.info("Reached end of voice stream");
				reachedEnd = true;
			}
			else if (count < 0 && !reachedStart)
			{
				count = 0;
			}
		}

		var rawAudioInputStream = new ByteArrayInputStream(rawAudioData.toByteArray());
		AudioInputStream input = new AudioInputStream(rawAudioInputStream, format, rawAudioData.size() / format.getFrameSize());
		formattedAudioData.reset();
		AudioSystem.write(input, AudioFileFormat.Type.WAVE, formattedAudioData);
		latch.countDown();
	}

	@SneakyThrows
	private String submitSound()
	{
		var builder = new MultipartBodyBuilder();
		builder.part("audio_file", formattedAudioData.toByteArray())
				.filename("clip.wav")
				.contentType(MediaType.valueOf("audio/wave"));
		var response = client.post()
				.uri("/asr?encode=true&task=transcribe&language=en&word_timestamps=false&output=txt")
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.bodyValue(builder.build())
				.retrieve()
				.toEntity(String.class)
				.block();
		log.info("Response: {}", response);
		if (response == null)
			throw new IllegalStateException("Response was null");
		return response.getBody();
	}
}
