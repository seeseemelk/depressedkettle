package be.seeseemelk.depressedkettle;

import io.github.amithkoujalgi.ollama4j.core.OllamaAPI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OllamaConfiguration
{
	@Value("${chat.api:http://localhost:11434/}")
	private String host;

	@Bean
	public OllamaAPI ollamaAPI()
	{
		var api = new OllamaAPI(host);
		api.setRequestTimeoutSeconds(120);
		api.setVerbose(true);
		if (!api.ping())
			throw new RuntimeException("Could not reach Ollama");
		return api;
	}
}
