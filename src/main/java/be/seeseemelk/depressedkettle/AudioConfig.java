package be.seeseemelk.depressedkettle;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

@Configuration
public class AudioConfig
{
	@Bean
	public AudioFormat audioFormat()
	{
		return new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100, 16, 2, 4, 44100, false);
	}

	@Bean
	public TargetDataLine microphone(AudioFormat format) throws LineUnavailableException
	{
		var line = AudioSystem.getTargetDataLine(format);
		line.open(format);
		return line;
	}
}
