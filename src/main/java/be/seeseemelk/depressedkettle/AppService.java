package be.seeseemelk.depressedkettle;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Scanner;

@Service
@Slf4j
public class AppService
{
	@Autowired
	private ChatService chat;
	@Autowired
	private TTSService tts;
	@Autowired
	private MqttService mqtt;
	@Autowired
	private WhisperService whisper;

	@EventListener(ApplicationReadyEvent.class)
	public void onStart()
	{
		var thread = new Thread(() ->
		{
			try
			{
				Scanner scanner = new Scanner(System.in);
				for (; ; )
				{
//					String line = scanner.nextLine();
					log.info("Listening...");
					whisper.startRecording();
					var line = whisper.autoStop();
					log.info("Processing...");
					if (line == null)
						continue;
					var response = chat.addUserChat(line);

					if (response.contains("{ON}"))
						mqtt.on();
					else if (response.contains("{OFF}"))
						mqtt.off();

					var ttsResponse = response
							.replace("{ON}", "")
							.replace("{OFF}", "")
							.replace("{NOP}", "")
							.replaceAll("\\*.*?\\*", "");
					log.info("Speaking {}", ttsResponse);
					tts.play(ttsResponse);
					log.info("Finished speaking");
				}
			}
			catch (Throwable e)
			{
				log.error("Fatal error occurred while running", e);
				System.exit(1);
			}
		});
		thread.start();
	}
}
